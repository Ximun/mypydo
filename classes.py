import sys
import os
import json
from xdg import XDG_DATA_HOME
from termcolor import colored

class Mypydo:
    """
        Main wrapper for the TODO list application.
        ===========================================

        Handle The JSON Todolist File (read, check and write),
        Create and Handle the Todolist Object,
        Create and Handle the Commandline Controller

        :param filename: name of the JSON file
        :type filename: str
    """

    def __init__(self, filename="todolist.json", args: list = []):
        self.folderpath = XDG_DATA_HOME.joinpath('mypydo')
        self.filepath = os.path.join(self.folderpath, filename)

        self.check_or_create('d', self.folderpath)
        self.check_or_create('f', self.filepath)
        self.read()

        self.todolist = TodoList(self.data)

        if len(args) == 1:
            View.display_list(self.data)

        if len(args) > 1:
            self.handle_cli(args)

    def check_or_create(self, type, path):
        """ Check or create if folder or file exists """
        if not os.path.exists(path):
            if type == 'd':
                os.makedirs(path)
            elif type == 'f':
                self.write({})

    def read(self):
        """ Read the JSON file content and save it in self.data as dict """
        with open(self.filepath, "r") as file:
            self.data = {}
            for key,value in json.load(file).items():
                self.data[int(key)] = value

    def write(self, data):
        """ Write data into the JSON file """
        with open(self.filepath, "w") as file:
            file.write(json.dumps(data, ensure_ascii=False, indent=2))

    # todo: créer une seule fonction ?
    def add(self, contents: list):
        for content in contents:
            self.todolist.add(str(content), False)
            self.write(self.data)

    def delete(self, ids: list):
        for id in ids:
            self.todolist.delete(int(id))
            self.write(self.data)

    def done(self, ids: list):
        for id in ids:
            try:
                self.todolist.done(int(id))
                self.write(self.data)
            except KeyError as e:
                print(e.strerror)

    def handle_cli(self, args):
        self.action = args[1]
        self.targets = args[2:]
        getattr(self, self.action)(self.targets)

class TodoList:
    """
        Data Worker for the Todolist
        ============================

        :param data: The todolist before treatment
        :type data: dict
        :return: The todolist after treatment
        :rtype: dict
    """

    def __init__(self, data: str):
        self.tasks = data

    def add(self, content: str, done: bool):
        """ Add a new task """
        new_task = {}
        new_task["content"] = content
        new_task["done"] = done

        if len(self.tasks) > 0:
            id = int(max(self.tasks.keys()))+1
            self.tasks[id] = new_task
        else:
            self.tasks[1] = new_task

    def delete(self, id):
        """ Delete a task """
        del self.tasks[id]

    def done(self, id):
        """ Check a task as done """
        self.tasks[id]["done"] = True

class View:

    def __init__(self, color: str, message: str, block: bool = False):
        print(colored(message, color))
        if block:
            sys.exit(0)

    @staticmethod
    def display_help():
        print("This is the help...")

    @staticmethod
    def display_list(list):
        for key, value in list.items():
            print(str(key) + "." +value["content"])
