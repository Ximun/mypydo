import unittest
import os
import json
import copy
from classes import TodoList

class TodoListActionsTest(unittest.TestCase):

    tasks = {
        1: {
            "content": "This is a first task",
            "done": False
        },
        2: {
            "content": "This is a second task",
            "done": True
        }
    }

    def test_add_task(self):
        todo = TodoList(self.tasks.copy())
        todo.add("New task", False)

        self.assertEqual(todo.tasks[3]["content"], "New task")
        self.assertEqual(todo.tasks[3]["done"], False)

    def test_delete_task(self):
        todo = TodoList(self.tasks.copy())
        todo.delete(2)
        self.assertEqual(todo.tasks[max(todo.tasks.keys())]["content"], "This is a first task")

    def test_done_task(self):
        todo = TodoList(self.tasks.copy())
        todo.done(1)
        self.assertEqual(todo.tasks[1]["done"], True)

if __name__ == '__main__':
    unittest.main()
